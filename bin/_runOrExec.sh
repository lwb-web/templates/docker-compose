#!/bin/bash

# Check for a docker .env file first
. "`dirname "$0"`/_checkEnv.sh"

# Default to Drupal container
if [ -z ${CONTAINER} ]; then
    CONTAINER="app"
fi

OPT=""
if [ "${COMPOSE_NON_INTERACTIVE}" = "1" ]; then
    OPT="-T"
fi

# Determine right task
if [ -n "`/opt/bin/docker-compose "${DOCKER_COMPOSE_FILES[@]}" ps | grep -e "_${CONTAINER}_.* Up "`" ]; then
    echo -e "Use \033[32mrunning\033[0m ${CONTAINER} container"
    TASK="exec"
else
    echo -e "Run in \033[31mnew\033[0m ${CONTAINER} container"
    TASK="run --no-deps --rm"
fi

if [ -z ${WITHBASH} ]; then
    /opt/bin/docker-compose "${DOCKER_COMPOSE_FILES[@]}" ${TASK} ${OPT} ${CONTAINER} ${CMD}
else
    /opt/bin/docker-compose "${DOCKER_COMPOSE_FILES[@]}" ${TASK} ${OPT} ${CONTAINER} bash -c "${CMD}"
fi
