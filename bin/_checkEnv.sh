#!/bin/bash

DIRNAME=`dirname "$0"`
DIR="${DIRNAME}"
if [ "`echo ${DIRNAME} | head -c1`" != "/" ]; then
    DIR="`pwd`/${DIRNAME}"
fi

while [ ! -e "${DIR}/docker-compose.yml" ]; do
    if [ "${DIR}" = "/" ]; then
        echo -e "\033[31mCould not find a docker-compose.yml\033[0m"
        exit 127
    fi
	
    # Note: if you want to ignore symlinks, use "$(realpath -s "$path"/..)"
    # Note: readlink -f would be nice, but will not work on MacOS
    cd "${DIR}/.."
    DIR=`pwd`
done

# Create .env when not existing
if [ ! -e "${DIR}/.env" ]; then
    cp "${DIR}/.env.dev" "${DIR}/.env"
    echo "" >> "${DIR}/.env"
    echo -e "Created \033[33mnew docker \033[1m.env\033[21m file\033[0m"
fi

# Load .env file
. "${DIR}/.env"
DOCKER_COMPOSE_FILES=(-f "${DIR}/docker-compose.yml" -f "${DIR}/docker-compose.${DOCKER_ENV:-dev}.yml")
