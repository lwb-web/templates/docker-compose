#!/bin/bash

BINDIR=$(cd `dirname "$0"` && pwd)/../bin

. ${BINDIR}/_checkEnv.sh
. ${BINDIR}/../.env

/bin/bash ${BINDIR}/drush -l "${DRUPAL_VIRTUAL_HOST}${DRUPAL_VIRTUAL_LOCATION} cron"

