#!/bin/bash

export COMPOSE_NON_INTERACTIVE=1

for APP_NAME in `ls /opt/apps`
do
  if [ -f /opt/apps/$APP_NAME/cron/cron-daily.sh ]; then
    echo "App: ${APP_NAME}"
    /bin/bash /opt/apps/${APP_NAME}/cron/cron-daily.sh
  fi
done

exit 0

