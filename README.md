# Docker-Compose Template

Das Template stellt den Rahmen für Docker bzw. [Docker-Compose](https://docs.docker.com/compose/) basierte Projekte der LWB. Es wurde für [CoreOS](https://kinvolk.io/flatcar-container-linux/) entwickelt, lässt sich aber für [Debian](https://www.debian.org/index.de.html) und [Ubuntu](https://ubuntu.com/) nutzen.

Im Template wird von einer Integration mit [Gitlab](https://about.gitlab.com/) und einem [Gitlab-Runner](https://docs.gitlab.com/runner/) mit Zugriff auf das Zielsystem ausgegangen. Ebenso wurde als Init-System [Systemd](https://wiki.ubuntuusers.de/systemd/) beschrieben. Andere Init-Systeme sind ebenfalls denkbar.

Das Template bietet nur einen Rahmen. Beispielsweise sind für Drupal-basierte Projekte Skripts zur Integration von Drush denkbar.

## Features

* Betrieb von Containern (Stacks) in verschiedenen Umgebungen, live, stage, dev
* Automatische Integration von Cronjobs und Backups in das Betriebssystem
* Automatisches Ausrollen im Zusammenspiel mit einem Gitlab-Runner
* Build vom Images und Upload in einen Hub

## Voraussetzungen

* [Docker](https://docs.docker.com/)
* [Docker-Compose](https://docs.docker.com/compose/), ein Aufruf über den Pfad `/opt/bin/docker-compose`
* Installation eines Service zum Starten und Beenden des Stacks, Beispiel für [Systemd](https://wiki.ubuntuusers.de/systemd/) liegt unter `_integration/service`

## Installation

Die Installation wird durch den Runner auf dem Zielsystem ausgeführt. Folgende Schritte wurden beispielhaft hinterlegt:

* Build der notwendigen / enthaltenen Images
* Upload der Images
* Stoppen des Service
* Kopieren des Projekte in den Ordner `/opt/apps/STACK_NAME` (der Stack-Name muss in den env-Datein und in .gitlab-ci.yml identisch sein)
* Bereitstellen der jeweiligen .env-Datei, Auswahl erfolgt durch Branch, z.B. für den Master-Branch wird .env.live nach .env kopiert
* Anreichern der .env-Datei mit weiteren Variablen aus anderen Quellen z.B. Passwörter aus den in Gitlab hinterlegten Variablen
* Start des Service
* im Gitlab wird ein Job hinterlegt, der zum manuellen Start eines Datenimports genutzt werden kann

## Stages

Das Template wurde für drei Stages ausgelegt.

* live (Produktion) `docker-compose.live.yml`
* stage (Test, QA) `docker-compose.stage.yml`
* dev (Entwicklung) `docker-compose.dev.yml`

Das entsprechende Docker-Compose-File (docker-compose.{STAGE}.yml) wird zusätzlich zum Docker-Compose-File `docker-compose.yml` gestartet.

## Cronjobs

Um verschiedene Aufgaben innerhalb von einzelnen Containern zu starten, dienen Cronjobs. Beipielhaft wurden Dateien für die Einrichtung in Systemd hinterlegt. `_intergration/cron`

Der Ablauf am Beispiel eines täglichen Cronjobs wird nachstehend erklärt. 

Systemd-Timer startet das Skript `/opt/apps/docker-compose-cron-daily.sh`. Das Skript scannt alle unter `/opt/apps` hinterlegten Stacks auf das Vorhandensein des entsprechenden Scripts `/opt/apps/STACK_NAME/cron/cron-daily.sh`. Wenn das Script existiert wird es ausgeführt.

Somit muss nicht für jeden Stack Cronjobs auf Betriebssystemebene installiert werden. Das Vorhandensein des entsprechenden Skripts ist ausreichend.

## Backup

Das Vorgehen ist zum denen für Cronjobs identisch. Eine Datei  `/opt/apps/APPLICATION_NAME/bin/backup` muss dafür vorhanden sein und der entsprechende Timer muss im Systemd angelegt und aktiviert sein.

Der Unterschied zum Cronjob besteht darin, dass das Backup weitere Container `docker-compose.backup.yml` startet, die das Backup ausführen.

## Weitere Skripts

* bin/import: Start des Imports über docker-compose.import.yml
* bin/logs: Ausgabe der Log-Daten
* bin/sh: Zugriff auf das Terminal des Containers, der im Stacks unter `app` gestartet wurde
* bin/update: Stop aller Container im Stack, Download oder Build notwendiger Images, Start der Container im Stack
